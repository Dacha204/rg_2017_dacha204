# Racunarska grafika 2017/2018 - OpenGL

Laboratorijske vezbe iz racunarske grafike.

## Kontrole / Navigacija

| Tasteri          | Opis          |
| -------------    |-------------|
| + -              | Zoom (kamera) |
| PgUp PgDown      | Zoom (alternativni)|
| Strelice         | Rotiranje oko tacke gledanja |
| WASD QE          | Pomeranje tacke gledanja |
| Num7 8 9 4 5 6 2 | Pomeranje tacke gledanja (alt) |
| Num0 Home        | Vracanje tacke gledanja u 0,0,0|
| O-P K-L N-M         | Menjanje uglova lampe |
| V                | Wire mode [toggle]      |
| SPACE            | Debug mode (coordsys) [toggle] |
