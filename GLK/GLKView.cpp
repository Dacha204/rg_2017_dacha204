
// GLKView.cpp : implementation of the CGLKView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "GLK.h"
#endif

#include "GLKDoc.h"
#include "GLKView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CGLKView

IMPLEMENT_DYNCREATE(CGLKView, CView)

BEGIN_MESSAGE_MAP(CGLKView, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEHWHEEL()
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()

// CGLKView construction/destruction

CGLKView::CGLKView()
{
	// TODO: add construction code here

}

CGLKView::~CGLKView()
{
}

BOOL CGLKView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CGLKView drawing

void CGLKView::OnDraw(CDC* pDC)
{
	CGLKDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	m_glRenderer.DrawScene(pDC);
}


// CGLKView printing

BOOL CGLKView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CGLKView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CGLKView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CGLKView diagnostics

#ifdef _DEBUG
void CGLKView::AssertValid() const
{
	CView::AssertValid();
}

void CGLKView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGLKDoc* CGLKView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGLKDoc)));
	return (CGLKDoc*)m_pDocument;
}
#endif //_DEBUG


// CGLKView message handlers


int CGLKView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CDC* pDC = GetDC();
	m_glRenderer.CreateGLContext(pDC);
	ReleaseDC(pDC);

	return 0;
}


void CGLKView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	CDC* pDC = GetDC();
	m_glRenderer.Reshape(pDC, cx, cy);
	ReleaseDC(pDC);
}


void CGLKView::OnDestroy()
{
	CView::OnDestroy();

	CDC* pDC = GetDC();
	m_glRenderer.DestroyScene(pDC);
	ReleaseDC(pDC);	
}


BOOL CGLKView::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
}


void CGLKView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	CDC* pDC = GetDC();
	m_glRenderer.PrepareScene(pDC);
	ReleaseDC(pDC);
}


void CGLKView::OnMouseHWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// This feature requires Windows Vista or greater.
	// The symbol _WIN32_WINNT must be >= 0x0600.
	// TODO: Add your message handler code here and/or call default

	CView::OnMouseHWheel(nFlags, zDelta, pt);
}


void CGLKView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default
	float moveStep = (float) 0.1;
	float angleStep = (float) 2.5;

	switch (nChar)
	{
	case VK_ADD:
	case VK_PRIOR: //PageUp
		m_glRenderer.CameraMove(CamDirectionZoom, -0.5);
		break;
	case VK_SUBTRACT:
	case VK_NEXT:	//PageDown
		m_glRenderer.CameraMove(CamDirectionZoom, 0.5);
		break;

	case VK_LEFT:
		m_glRenderer.CameraMove(CamDirectionHorizontal,angleStep);
		break;
	case VK_RIGHT:
		m_glRenderer.CameraMove(CamDirectionHorizontal, -angleStep);
		break;

	case VK_UP:
		m_glRenderer.CameraMove(CamDirectionVertical, angleStep);
		break;
	case VK_DOWN:
		m_glRenderer.CameraMove(CamDirectionVertical, -angleStep);
		break;

	case VK_NUMPAD7:
	case 'Q':
		m_glRenderer.LookAtMove(AxisY,moveStep);
		break;
	case VK_NUMPAD9:
	case 'E':
		m_glRenderer.LookAtMove(AxisY, -moveStep);
		break;

	case VK_NUMPAD8:
	case 'W':
		m_glRenderer.LookAtMove(AxisZ, -moveStep);
		break;
	
	case VK_NUMPAD5:
	case VK_NUMPAD2:
	case 'S':
		m_glRenderer.LookAtMove(AxisZ, moveStep);
		break;

	case VK_NUMPAD4:
	case 'A':
		m_glRenderer.LookAtMove(AxisX, -moveStep);
		break;

	case VK_NUMPAD6:
	case 'D':
		m_glRenderer.LookAtMove(AxisX, moveStep);
		break;
	
	case VK_NUMPAD0:
	case VK_HOME:
		m_glRenderer.LookAtReset();
		break;


	case 'N':
		m_glRenderer.LampAngle1 += angleStep;
		break;
	case 'M':
		m_glRenderer.LampAngle1 -= angleStep;
		break;

	case 'K':
		m_glRenderer.LampAngle2 += angleStep;
		break;
	case 'L':
		m_glRenderer.LampAngle2 -= angleStep;
		break;

	case 'O':
		m_glRenderer.LampAngle3 += angleStep;
		break;
	case 'P':
		m_glRenderer.LampAngle3 -= angleStep;
		break;

	case VK_SPACE:
		m_glRenderer.MYDEBUG_ENABLED = !m_glRenderer.MYDEBUG_ENABLED;
		break;
	case 'V':
		m_glRenderer.MYDEBUG_WIRE = !m_glRenderer.MYDEBUG_WIRE;
		break;
	default:
		break;
	}

	Invalidate();

	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}
