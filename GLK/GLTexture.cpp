#include "stdafx.h"
#include "GLTexture.h"
#include "GL\GL.H"
#include "DImage.h"
#include "GL\GLU.H"

void GLTexture::PrepareTexturing(bool bEnableLighting)
{
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	
	if (bEnableLighting)
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	else
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

GLTexture::GLTexture()
{
	m_ID = 0;
}

GLTexture::~GLTexture()
{
	Release();
}

void GLTexture::LoadFromFile(CString filename)
{
	Release();

	DImage textureImage;
	textureImage.Load(filename);

	glGenTextures(1, &m_ID);
	glBindTexture(GL_TEXTURE_2D, m_ID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, textureImage.Width(), textureImage.Height(), 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, textureImage.GetDIBBits());
}

void GLTexture::Select()
{
	if (!IsInitialized())
		return;

	glBindTexture(GL_TEXTURE_2D, m_ID);	
}

void GLTexture::Release()
{
	if (!IsInitialized())
		return;

	glDeleteTextures(1, &m_ID);
	m_ID = 0;	
}

inline bool GLTexture::IsInitialized()
{
	return m_ID != 0;
}
