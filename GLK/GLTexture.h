#pragma once
class GLTexture
{
public:
	static void PrepareTexturing(bool bEnableLighting);

private:
	unsigned int m_ID;

public:
	GLTexture();
	~GLTexture();
	
	void LoadFromFile(CString filename);
	void Select();
	void Release();

	inline bool IsInitialized();
};

