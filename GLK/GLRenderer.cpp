#include "StdAfx.h"
#include "GLRenderer.h"
#include "GL\gl.h"
#include "GL\glu.h"
#include "GL\glaux.h"
#include "GL\glut.h"
//#pragma comment(lib, "GL\\glut32.lib")

#define _USE_MATH_DEFINES
#include <math.h>

#define _TEXTURE_LIGHTING true

#define _FILE_TEX_WOOD "ASHSEN512.bmp"
#define _FILE_TEX_WALL "Wall512.bmp"
#define _FILE_TEX_FLOOR "PAT39.bmp"
//#define _FILE_TEX_WOOD "ASHSEN512_debug.bmp"
//#define _FILE_TEX_WALL "Wall512_debug.bmp"
//#define _FILE_TEX_FLOOR "PAT39_debug.bmp"


double Radiansd(float angle)
{
	return (double)(M_PI / 180)*angle;
}
double Radiansd(double angle)
{
	return (double)(M_PI / 180)*angle;
}
float Radiansf(float angle)
{
	return (float)(M_PI / 180)*angle;
}

CGLRenderer::CGLRenderer(void)
{
	m_pointLookAt = { 0,0,0 };
	m_camera = { 50, 25, 5.5 };

	UpdateCamera();

	LampAngle1 = 30;
	LampAngle2 = -75;
	LampAngle3 = -110;
}
CGLRenderer::~CGLRenderer(void)
{
}

bool CGLRenderer::CreateGLContext(CDC* pDC)
{
	PIXELFORMATDESCRIPTOR pfd ;
   	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
   	pfd.nSize  = sizeof(PIXELFORMATDESCRIPTOR);
   	pfd.nVersion   = 1; 
   	pfd.dwFlags    = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;   
   	pfd.iPixelType = PFD_TYPE_RGBA; 
   	pfd.cColorBits = 32;
   	pfd.cDepthBits = 24; 
   	pfd.iLayerType = PFD_MAIN_PLANE;
	
	int nPixelFormat = ChoosePixelFormat(pDC->m_hDC, &pfd);
	
	if (nPixelFormat == 0) return false; 

	BOOL bResult = SetPixelFormat (pDC->m_hDC, nPixelFormat, &pfd);
  	
	if (!bResult) return false; 

   	m_hrc = wglCreateContext(pDC->m_hDC); 

	if (!m_hrc) return false; 

	return true;	
}
void CGLRenderer::PrepareScene(CDC *pDC)
{
	wglMakeCurrent(pDC->m_hDC, m_hrc);
	//---------------------------------
	
	glClearColor(1, 1, 1, 0);
	//glEnable(GL_CULL_FACE);
	
	SetLightModel();

	InitWoodMaterial();
	InitLampMaterial();
	InitFloorMaterial();
	InitWallMaterial();
	InitBulbMaterial();

	PrepareTextures();
	//---------------------------------
	wglMakeCurrent(NULL, NULL);
}
void CGLRenderer::Reshape(CDC *pDC, int w, int h)
{
	wglMakeCurrent(pDC->m_hDC, m_hrc);
	//---------------------------------
	
	glViewport(0, 0, w, h);
	
	double aspect = (double)w / (double)h;

	glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(40, aspect, 0.1, 100);
	glMatrixMode(GL_MODELVIEW);
	
	//---------------------------------
	wglMakeCurrent(NULL, NULL);
}
void CGLRenderer::DestroyScene(CDC *pDC)
{
	wglMakeCurrent(pDC->m_hDC, m_hrc);
	
	m_textureWall.Release();
	m_textureFloor.Release();
	m_textureWood.Release();

	wglMakeCurrent(NULL,NULL); 
	if(m_hrc) 
	{
		wglDeleteContext(m_hrc);
		m_hrc = NULL;
	}
}

void CGLRenderer::DrawScene(CDC *pDC)
{
	wglMakeCurrent(pDC->m_hDC, m_hrc);
	//---------------------------------
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	gluLookAt(
		m_pointEye.X, m_pointEye.Y, m_pointEye.Z,
		m_pointLookAt.X, m_pointLookAt.Y, m_pointLookAt.Z,
		0, 1, 0
	);
	
	DEBUG_DrawCoordSys();
	DEBUG_DrawLookAtPosition();
	DEBUG_WireMode();

	glColor3f(0, 0.48, 0.8);		//Default color for drawing
	SetLightMain(0.25, 4, 1 );		//Globa/Directional lighting
	
	/* ============== DRAWING PART ================= */

	#pragma region NonTextured Drawing
	//glDisable(GL_TEXTURE_2D);
	//glPushMatrix();
	//	glTranslated(0.9 + 0.25, 0.83, 0.9 + 0.40);
	//	glScaled(0.5, 0.5, 0.5);
	//	DrawLamp();
	//glPopMatrix();
	//
	//glPushMatrix();
	//	glTranslated(0.9, 0, 0.9);
	//	glScaled(1.2, 1.2, 1.2);
	//	DrawTable();
	//glPopMatrix();

	//DrawWalls();
	#pragma endregion

	#pragma region TexturedDrawing
	glPushMatrix();
		glTranslated(0.9 + 0.25, 0.83, 0.9 + 0.40);
		glScaled(0.5, 0.5, 0.5);
		DrawLamp();
	glPopMatrix();
	
	glPushMatrix();
		glTranslated(0.9, 0, 0.9);
		glScaled(1.2, 1.2, 1.2);
		DrawTexturedTable();
	glPopMatrix();

	glPushMatrix();
		glTranslated(2.7, 0, 1.5);
		DrawTexturedCylinder(0.25, 0.75, 25);
	glPopMatrix();

	DrawTexturedWalls();
	#pragma endregion

	//---------------------------------
	glFlush();
	SwapBuffers(pDC->m_hDC);
	wglMakeCurrent(NULL, NULL);
}

void CGLRenderer::DrawBoxQS(double a, double b, double c) //QUADSTRIP
{
	double ah = a / 2, bh = b / 2, ch = c / 2;

	glBegin(GL_QUAD_STRIP);
		// Front left vertical edge
		glVertex3d(-ah,  bh, ch);
		glVertex3d(-ah, -bh, ch);

		// Front right vertical edge
		glVertex3d(ah,  bh, ch);
		glVertex3d(ah, -bh, ch);

		// Back right vertical edge 
		glVertex3d(ah,  bh, -ch);
		glVertex3d(ah, -bh, -ch);

		// Back right vertical edge 
		glVertex3d(-ah,  bh, -ch);
		glVertex3d(-ah, -bh, -ch);

		// Front left vertical edge
		glVertex3d(-ah,  bh, ch);
		glVertex3d(-ah, -bh, ch);
	glEnd();

	glBegin(GL_QUADS);
		//UP
		glVertex3d(-ah, bh,  ch);
		glVertex3d( ah, bh,  ch);
		glVertex3d( ah, bh, -ch);
		glVertex3d(-ah, bh, -ch);

		//DOWN
		glVertex3d(-ah, -bh, -ch);
		glVertex3d( ah, -bh, -ch);
		glVertex3d( ah, -bh,  ch);
		glVertex3d(-ah, -bh,  ch);
	glEnd();

}
void CGLRenderer::DrawBox(double a, double b, double c, int slicesX, int slicesY) //QUADS
{
	//Front
	glPushMatrix();
		glTranslated(0, 0, c / 2);
		DrawRectangle(a, b, slicesX, slicesY);
	glPopMatrix();
	
	//Right
	glPushMatrix();
		glRotated(90, 0, 1, 0);
		glTranslated(0, 0, a / 2);
		DrawRectangle(c, b, slicesX, slicesY);
	glPopMatrix();

	//Back
	glPushMatrix();
		glRotated(180, 0, 1, 0);
		glTranslated(0, 0, c / 2);
		DrawRectangle(a, b, slicesX, slicesY);
	glPopMatrix();

	//Left
	glPushMatrix();
		glRotated(-90, 0, 1, 0);
		glTranslated(0, 0, a / 2);
		DrawRectangle(c, b, slicesX, slicesY);
	glPopMatrix();

	//Top
	glPushMatrix();
		glRotated(-90, 1, 0, 0);
		glTranslated(0, 0, b / 2);
		DrawRectangle(a, c, slicesX, slicesY);
	glPopMatrix();

	//Bottom
	glPushMatrix();
		glRotated(90, 1, 0, 0);
		glTranslated(0, 0, b / 2);
		DrawRectangle(a, c, slicesX, slicesY);
	glPopMatrix();
	
}

void CGLRenderer::DrawLamp()
{
	float k = 0.01;
	float stickBaseHeight = k * 85;
	float stickWidth = k * 5;

	float lampHeadWidth = k * 9;
	float lampHeadHeight = lampHeadWidth * 3;
	float lampHeadRadius = k * 12;

	GLMaterial oldMat;
	oldMat.LoadCurrent();
	m_materialLamp.Select();

	glPushAttrib(GL_CURRENT_BIT);

	glPushMatrix();
		glRotated(LampAngle1, 0, 0, 1);
		glTranslated(0, stickBaseHeight, 0);
		glRotated(LampAngle2, 0, 0, 1);
		glTranslated(0, stickBaseHeight, 0);
		glRotated(LampAngle3, 0, 0, 1);
		glTranslated(0, lampHeadHeight / 2, 0);
		glTranslated(0, lampHeadRadius / 2, 0);
		glTranslated(0, -lampHeadRadius / 4, 0);
		SetLightLamp();
		DEBUG_DrawLight();
	glPopMatrix();

	glPushMatrix();
		//base sphere
		glColor3f(1, 0, 0);
		glutSolidSphere(k*15, 60, 60);
		
		//base stick
		glRotated(LampAngle1, 0, 0, 1);
		glTranslated(0, stickBaseHeight / 2, 0);
		DrawBox(stickWidth, stickBaseHeight, stickWidth, 1, 5);
		
		//joint sphere
		glColor3f(0, 0, 1);
		glTranslated(0, stickBaseHeight / 2, 0);
		glutSolidSphere(k * 5, 60, 60);
		
		//joint stick
		glColor3f(1, 0, 0);
		glRotated(LampAngle2, 0, 0, 1);
		glTranslated(0, stickBaseHeight / 2, 0);
		DrawBox(stickWidth, stickBaseHeight, stickWidth, 1, 5);
		
		//lamp head rect
		glColor3f(0, 0, 1);
		glTranslated(0, stickBaseHeight / 2, 0);
		glRotated(LampAngle3, 0, 0, 1);
		DrawBox(lampHeadWidth, lampHeadHeight, lampHeadWidth, 1, 5);
		
		//lamp head sphere
		glTranslated(0, lampHeadHeight / 2, 0);
		glTranslated(0, lampHeadRadius / 2, 0);

		GLdouble eqn[4] = { 0.0, -1.0, 0.0, +0.01 };
		glClipPlane(GL_CLIP_PLANE0, eqn);
		glTranslated(0, -lampHeadRadius / 4, 0);
		glEnable(GL_CLIP_PLANE0);
		glutSolidSphere(lampHeadRadius, 100, 100);
		glDisable(GL_CLIP_PLANE0);
		
		glColor3f(1, 1, 1);
		m_materialBulb.Select();
		glutSolidSphere(lampHeadRadius / 3, 10, 10);
		//SetLightLamp();
		oldMat.Select();
		DEBUG_DrawCoordSys();
		
		DEBUG_DrawLight();
		
	glPopMatrix();
	glPopAttrib();
	oldMat.Select();
}
void CGLRenderer::DrawTable()
{
	//Drawing in 1x1 box;

	double legColor[4] = { 0.0, 1.0, 0.0, 0.0 };
	double tableTopColor[4] = { 0.8, 0.5, 0.5, 0.0 };
	double tableBottomColor[4] = { 0.6, 0.3, 0.3, 0.0 };

	double k = 0.01;
	double legWidth = k*2;
	double legHeight = k*65;

	double tableTopWidth = k * 100;
	double tableTopDepth = k * 66;
	double tableTopHeight = k*5;

	double tableBottomWidth = k * 90;
	double tableBottomDepth = k * 56;
	double tableBottomHeight = k*10;

	GLMaterial oldMat;
	oldMat.LoadCurrent();
	m_materialWood.Select();

	glPushAttrib(GL_CURRENT_BIT);
	glPushMatrix();
		glTranslated(tableTopWidth/2, 0, tableTopDepth/2);
		
		//Bottom table
		glPushMatrix();
			glColor4dv(tableBottomColor);
			glTranslated(0, legHeight - (tableBottomHeight/2), 0);
			DrawBox(tableBottomWidth, tableBottomHeight, tableBottomDepth, GranularityX,GranularityY);
		glPopMatrix();
	
		//Legs
		glPushMatrix();
			glTranslated(0, legHeight / 2, 0);
			glColor4dv(legColor);

			//left bottom
			glPushMatrix();
				glTranslated(-(tableBottomWidth/2),0 , -(tableBottomDepth/2));
				DrawBox(legWidth, legHeight, legWidth, 1,1);
			glPopMatrix();

			//left top
			glPushMatrix();
				glTranslated(-(tableBottomWidth / 2), 0, (tableBottomDepth / 2));
				DrawBox(legWidth, legHeight, legWidth, 1, 1);
			glPopMatrix();
	
			//right bottom
			glPushMatrix();
				glTranslated((tableBottomWidth / 2), 0, -(tableBottomDepth / 2));
				DrawBox(legWidth, legHeight, legWidth, 1, 1);
			glPopMatrix();

			//right top
			glPushMatrix();
				glTranslated((tableBottomWidth / 2), 0, (tableBottomDepth / 2));
				DrawBox(legWidth, legHeight, legWidth, 1, 1);
			glPopMatrix();
		glPopMatrix();

		//Top table
		glPushMatrix();
			glColor4dv(tableTopColor);
			glTranslated(0, legHeight + (tableTopHeight / 2), 0);
			DrawBox(tableTopWidth, tableTopHeight, tableTopDepth, GranularityX, GranularityY);
		glPopMatrix();
	
	glPopMatrix();
	glPopAttrib();
	oldMat.Select();
}

void CGLRenderer::DrawRectangle(double width, double height, int slicesW, int slicesH)
{
	double stepW = width / slicesW;
	double stepH = height / slicesH;
	double h_cur = height / 2;
	double w_cur = -width / 2;

	float n_vect[3] = { 0,0,1 };
	glNormal3fv(n_vect);

	for (int h = 0; h < slicesH; h++, h_cur -= stepH)
	{
		w_cur = -width / 2;;
		glBegin(GL_QUAD_STRIP);
		for (int w = 0; w <= slicesW; w++, w_cur += stepW)
		{
			glVertex3d(w_cur, h_cur, 0);
			glVertex3d(w_cur, h_cur - stepH, 0);
		}
		glEnd();
	}
}
void CGLRenderer::DrawWall(double wallSize)
{
	glPushMatrix();
		glTranslated(wallSize / 2, wallSize / 2, 0);
		DrawRectangle(wallSize, wallSize, GranularityX, GranularityY);
	glPopMatrix();
}
void CGLRenderer::DrawWalls(double wallsSize)
{
	double ColorsXWall[4] = { 0.8, 0.8, 0.8, 1.0 };
	double ColorsYWall[4] = { 0.7, 0.7, 0.7, 1.0 };
	double ColorsZWall[4] = { 0.6 ,0.6, 0.6, 1.0 };

	GLMaterial oldMat;
	oldMat.LoadCurrent();
	
	glPushAttrib(GL_CURRENT_BIT);
	
	m_materialWall.Select();
	
	//X Wall
	glPushMatrix();
		glColor4dv(ColorsXWall);
		DrawWall(wallsSize);
	glPopMatrix();

	//Y Wall
	glPushMatrix();
		glColor4dv(ColorsYWall);
		glRotated(90, 0, 1, 0);
		glTranslated(-wallsSize, 0, 0);
		DrawWall(wallsSize);
	glPopMatrix();

	//Z Wall - Floor
	m_materialFloor.Select();
	glPushMatrix();
		glColor4dv(ColorsZWall);
		glRotated(-90, 0, 1, 0);
		glRotated(-90, 1, 0, 0);
		DrawWall();
	glPopMatrix();
	
	glPopAttrib();
	oldMat.Select();
}

#pragma region Textured Drawing
void CGLRenderer::DrawTexturedRect(double dSizeX, double dSizeY, int nSlicesX, int nSlicesY, double nRepeatS, double nRepeatT)
{
	bool wasTextureEnabled = glIsEnabled(GL_TEXTURE_2D);
	glEnable(GL_TEXTURE_2D);
	
	double sliceSizeX = dSizeX / nSlicesX;			double sliceSizeY = dSizeY / nSlicesY;
	double sliceTexSizeS = nRepeatS / nSlicesX;		double sliceTexSizeT = nRepeatT / nSlicesY;
	
	double initPositionX = -dSizeX / 2;				double initPositionY = dSizeY / 2;
	double initPositionTexS = 0;					double initPositionTexT = 1;

	double currentPositionX = initPositionX;		double currentPositionY = initPositionY;
	double currentTexPositionS = initPositionTexS;	double currentTexPositionT = initPositionTexT;

	glNormal3f(0, 0, 1);
	for (int yCurrentEdge = 0; yCurrentEdge < nSlicesY; yCurrentEdge++)
	{
		currentPositionX = initPositionX;
		currentTexPositionS = initPositionTexS;

		glBegin(GL_QUAD_STRIP);
		for (int xCurrentEdge = 0; xCurrentEdge <= nSlicesX; xCurrentEdge++)
		{
			glTexCoord2d(currentTexPositionS, currentTexPositionT);
			glVertex3d(currentPositionX, currentPositionY, 0);

			glTexCoord2d(currentTexPositionS, currentTexPositionT - sliceTexSizeT);
			glVertex3d(currentPositionX, currentPositionY - sliceSizeY, 0);

			currentPositionX += sliceSizeX;
			currentTexPositionS += sliceTexSizeS;
		}
		glEnd();

		currentPositionY -= sliceSizeY;
		currentTexPositionT -= sliceTexSizeT;
	}

	if (!wasTextureEnabled)
		glDisable(GL_TEXTURE_2D);
}
void CGLRenderer::DrawTexturedTableTop(double a, double b, double c, int slicesX, int slicesY)
{
	m_textureWood.Select();

	//Front
	glPushMatrix();
	glTranslated(0, 0, c / 2);
	DrawRectangle(a, b, slicesX, slicesY);
	glPopMatrix();

	//Right
	glPushMatrix();
	glRotated(90, 0, 1, 0);
	glTranslated(0, 0, a / 2);
	DrawRectangle(c, b, slicesX, slicesY);
	glPopMatrix();

	//Back
	glPushMatrix();
	glRotated(180, 0, 1, 0);
	glTranslated(0, 0, c / 2);
	DrawRectangle(a, b, slicesX, slicesY);
	glPopMatrix();

	//Left
	glPushMatrix();
	glRotated(-90, 0, 1, 0);
	glTranslated(0, 0, a / 2);
	DrawRectangle(c, b, slicesX, slicesY);
	glPopMatrix();

	//Top
	glPushMatrix();
	glRotated(-90, 1, 0, 0);
	glTranslated(0, 0, b / 2);
	
	DrawTexturedRect(a, c, slicesX, slicesY,1,1);
	glPopMatrix();

	//Bottom
	glPushMatrix();
	glRotated(90, 1, 0, 0);
	glTranslated(0, 0, b / 2);
	DrawRectangle(a, c, slicesX, slicesY);
	glPopMatrix();
}
void CGLRenderer::DrawTexturedTable()
{
	//Drawing in 1x1 box;

	double legColor[4] = { 0.0, 1.0, 0.0, 0.0 };
	double tableTopColor[4] = { 0.8, 0.5, 0.5, 0.0 };
	double tableBottomColor[4] = { 0.6, 0.3, 0.3, 0.0 };

	double k = 0.01;
	double legWidth = k * 2;
	double legHeight = k * 65;

	double tableTopWidth = k * 100;
	double tableTopDepth = k * 66;
	double tableTopHeight = k * 5;

	double tableBottomWidth = k * 90;
	double tableBottomDepth = k * 56;
	double tableBottomHeight = k * 10;

	GLMaterial oldMat;
	oldMat.LoadCurrent();
	m_materialWood.Select();

	glPushAttrib(GL_CURRENT_BIT);
	glPushMatrix();
	glTranslated(tableTopWidth / 2, 0, tableTopDepth / 2);

	//Bottom table
	glPushMatrix();
	glColor4dv(tableBottomColor);
	glTranslated(0, legHeight - (tableBottomHeight / 2), 0);
	DrawBox(tableBottomWidth, tableBottomHeight, tableBottomDepth, GranularityX, GranularityY);
	glPopMatrix();

	//Legs
	glPushMatrix();
	glTranslated(0, legHeight / 2, 0);
	glColor4dv(legColor);

	//left bottom
	glPushMatrix();
	glTranslated(-(tableBottomWidth / 2), 0, -(tableBottomDepth / 2));
	DrawBox(legWidth, legHeight, legWidth, 1, 1);
	glPopMatrix();

	//left top
	glPushMatrix();
	glTranslated(-(tableBottomWidth / 2), 0, (tableBottomDepth / 2));
	DrawBox(legWidth, legHeight, legWidth, 1, 1);
	glPopMatrix();

	//right bottom
	glPushMatrix();
	glTranslated((tableBottomWidth / 2), 0, -(tableBottomDepth / 2));
	DrawBox(legWidth, legHeight, legWidth, 1, 1);
	glPopMatrix();

	//right top
	glPushMatrix();
	glTranslated((tableBottomWidth / 2), 0, (tableBottomDepth / 2));
	DrawBox(legWidth, legHeight, legWidth, 1, 1);
	glPopMatrix();
	glPopMatrix();

	//Top table
	glPushMatrix();
	glColor4dv(tableTopColor);
	glTranslated(0, legHeight + (tableTopHeight / 2), 0);
	DrawTexturedTableTop(tableTopWidth, tableTopHeight, tableTopDepth, GranularityX, GranularityY);
	glPopMatrix();

	glPopMatrix();
	glPopAttrib();
	oldMat.Select();
}
void CGLRenderer::DrawTexturedWall(double wallSize)
{
	glPushMatrix();
		glTranslated(wallSize / 2, wallSize / 2, 0);
		DrawTexturedRect(wallSize, wallSize, GranularityX, GranularityY, 2,2);
	glPopMatrix();
}
void CGLRenderer::DrawTexturedWalls(double wallsSize)
{
	double ColorsXWall[4] = { 0.8, 0.8, 0.8, 1.0 };
	double ColorsYWall[4] = { 0.7, 0.7, 0.7, 1.0 };
	double ColorsZWall[4] = { 0.6 ,0.6, 0.6, 1.0 };

	GLMaterial oldMat;
	oldMat.LoadCurrent();
	glPushAttrib(GL_CURRENT_BIT);

	m_materialWall.Select();
	m_textureWall.Select();
	//X Wall
	glPushMatrix();
		glColor4dv(ColorsXWall);
		DrawTexturedWall(wallsSize);
	glPopMatrix();

	//Y Wall
	glPushMatrix();
		glColor4dv(ColorsYWall);
		glRotated(90, 0, 1, 0);
		glTranslated(-wallsSize, 0, 0);
		DrawTexturedWall(wallsSize);
	glPopMatrix();

	//Z Wall - Floor
	m_materialFloor.Select();
	m_textureFloor.Select();

	glPushMatrix();
		glColor4dv(ColorsZWall);
		glRotated(-90, 0, 1, 0);
		glRotated(-90, 1, 0, 0);
		DrawTexturedFloor(wallsSize);
	glPopMatrix();

	glPopAttrib();
	oldMat.Select();
}
void CGLRenderer::DrawTexturedFloor(double size)
{
	glPushMatrix();
		glTranslated(size / 2, size / 2, 0);
		DrawTexturedRect(size, size, GranularityX, GranularityY, 10, 10);
	glPopMatrix();
}
void CGLRenderer::DrawTexturedCylinder(double Radius, double Height, int nSides)
{
	bool wasTextureEnabled = glIsEnabled(GL_TEXTURE_2D);
	if (!wasTextureEnabled)
		glEnable(GL_TEXTURE_2D);

	double angleBaseTrianglePart = 360.0 / nSides;
	double currentAngle, currentAngleRadians, cosAngle, sinAngle;

	m_textureFloor.Select();
	m_materialFloor.Select();

	glPushMatrix();
	//Draw Top
	glBegin(GL_TRIANGLE_FAN);
		
		currentAngle = 0;
		glNormal3d(0.0, 1.0, 0.0);
		glTexCoord2d(0.5, 0.5);
		glVertex3d(0.0, Height, 0.0);

		for (int corner = 0; corner <= nSides; corner++)
		{
			currentAngleRadians = Radiansd(currentAngle);
			cosAngle = cos(currentAngleRadians);
			sinAngle = sin(currentAngleRadians);

			glNormal3d(0, 1, 0);
			glTexCoord2d(0.5 + cosAngle, 0.5 + sinAngle);
			glVertex3d(Radius * cosAngle, Height, Radius * sinAngle);

			currentAngle -= angleBaseTrianglePart;
		}
	glEnd();

	//Draw Bottom
	glBegin(GL_TRIANGLE_FAN);
		currentAngle = 0;
		glNormal3d(0.0, -1.0, 0.0);
		glTexCoord2d(0.5, 0.5);
		glVertex3d(0.0, 0.0, 0.0);
		for (int corner = 0; corner <= nSides; corner++)
		{
			currentAngleRadians = Radiansd(currentAngle);
			cosAngle = cos(currentAngleRadians);
			sinAngle = sin(currentAngleRadians);

			glNormal3d(0, -1, 0);
			glTexCoord2d(0.5 + cosAngle/2, 0.5 + sinAngle/2);
			glVertex3d(Radius * cosAngle, 0, Radius * sinAngle);

			currentAngle += angleBaseTrianglePart;
		}
	glEnd();

	//Draw Sides

	m_textureWood.Select();
	m_materialWood.Select();
	double sTexIndex = 0.0;

	glBegin(GL_QUAD_STRIP);
		currentAngle = 0;
		for (int corner = 0; corner <= nSides; corner++)
		{
			currentAngleRadians = Radiansd(currentAngle);
			cosAngle = cos(currentAngleRadians);
			sinAngle = sin(currentAngleRadians);

			glNormal3d(cosAngle, 0, sinAngle);
			
			glTexCoord2d(sTexIndex, 0.0);
			glVertex3d(Radius * cosAngle, 0, Radius * sinAngle);

			glTexCoord2d(sTexIndex, 1.0);
			glVertex3d(Radius * cosAngle, Height, Radius * sinAngle);

			sTexIndex += 1.0 / (nSides) * 4;
			currentAngle += angleBaseTrianglePart;
		}
	glEnd();
	
	glPopMatrix();
	if (!wasTextureEnabled)
		glDisable(GL_TEXTURE_2D);
}
#pragma endregion

/* ============ INITIALIZERS ===============*/
void CGLRenderer::SetLightModel()
{
	float model_ambient[] = { 0.2, 0.2, 0.2, 1.0 };

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,		 model_ambient);
	glLightModeli (GL_LIGHT_MODEL_LOCAL_VIEWER,	 GL_FALSE);
	glLightModeli (GL_LIGHT_MODEL_TWO_SIDE,		 GL_TRUE);
	
	glEnable(GL_LIGHTING);
}
void CGLRenderer::SetLightMain(double x, double y, double z)
{
	float light_ambient[]  = { 0.2, 0.2, 0.2, 1.0 };
	float light_diffuse[]  = { 1.0, 1.0, 1.0, 1.0 };
	float light_specular[] = { 1.0, 1.0, 1.0, 1.0 };

	glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	
	float light_pos[] = { x, y, z , 0 };	//Directional
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
	glEnable(GL_LIGHT0);

	DEBUG_DrawLight(x,y,z);
}
void CGLRenderer::SetLightLamp(double x, double y, double z)
{
	float light_ambient[] = { 0.95, 0.82, 0.25, 1.0 };
	float light_diffuse[] = { 0.95, 0.82, 0.25, 1.0 };
	float light_specular[] = { 0.3, 0.3, 0.3, 1.0 };

	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);

	float light_pos[] = { x, y, z , 1 };	//Positinal
	glLightfv(GL_LIGHT1, GL_POSITION, light_pos);
	
	float light_direction[] = { 0,1,0};
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light_direction);
	glLightf (GL_LIGHT1, GL_SPOT_EXPONENT, 4.0);
	glLightf (GL_LIGHT1, GL_SPOT_CUTOFF, 40);
	
	glEnable(GL_LIGHT1);
	DEBUG_DrawLight(x,y,z);
}
void CGLRenderer::InitWoodMaterial()
{
	m_materialWood.SetAmbient(0.35, 0.25, 0.2, 1.0);
	m_materialWood.SetDiffuse(0.45, 0.35, 0.3, 1.0);
	m_materialWood.SetEmission(0.0, 0.0, 0.0, 1.0);
	m_materialWood.SetSpecular(1, 1, 1, 1);
	m_materialWood.SetShininess(45);
}
void CGLRenderer::InitLampMaterial()
{
	m_materialLamp.SetAmbient(0.62, 0.0, 0.0, 1.0);
	m_materialLamp.SetDiffuse(0.62, 0.0, 0.0, 1.0);
	m_materialLamp.SetSpecular(0.1, 0.1, 0.1, 1.0);
	m_materialLamp.SetShininess(3.3);
}
void CGLRenderer::InitFloorMaterial()
{
	m_materialFloor.SetAmbient(0.5, 0.43, 0.33, 1.0);
	m_materialFloor.SetDiffuse(0.75, 0.60, 0.40, 1.0);
	m_materialFloor.SetEmission(0.0, 0.0, 0.0, 1.0);
	m_materialFloor.SetShininess(100);
}
void CGLRenderer::InitWallMaterial()
{
	m_materialWall.SetAmbient(0.4, 0.4, 0.4, 1.0);
	m_materialWall.SetDiffuse(0.8, 0.8, 0.8, 1.0);
	m_materialWall.SetEmission(0.0, 0.0, 0.0, 1.0);
	m_materialWall.SetShininess(100);
}
void CGLRenderer::InitBulbMaterial()
{
	m_materialBulb.SetAmbient(0.95, 0.85, 0.25, 1.0);
	m_materialBulb.SetDiffuse(0.95, 0.85, 0.25, 1.0);
	m_materialBulb.SetEmission(0.95, 0.85, 0.25, 1);
	m_materialBulb.SetSpecular(0, 0, 0, 1.0);
	m_materialBulb.SetShininess(0);
}
void CGLRenderer::PrepareTextures()
{
	GLTexture::PrepareTexturing(_TEXTURE_LIGHTING);

	if (!m_textureWall.IsInitialized())
		m_textureWall.LoadFromFile(CString(_FILE_TEX_WALL));

	if (!m_textureFloor.IsInitialized())
		m_textureFloor.LoadFromFile(CString(_FILE_TEX_FLOOR));

	if (!m_textureWood.IsInitialized())
		m_textureWood.LoadFromFile(CString(_FILE_TEX_WOOD));
}

/* ============ UTILITIES ===============*/
void CGLRenderer::UpdateCamera()
{
	m_pointEye.X = cosf(Radiansf(m_camera.CamAngleH)) * cosf(Radiansf(m_camera.CamAngleV)) * m_camera.CamDistance + m_pointLookAt.X;
	m_pointEye.Y = sinf(Radiansf(m_camera.CamAngleV)) * m_camera.CamDistance + m_pointLookAt.Y;
	m_pointEye.Z = sinf(Radiansf(m_camera.CamAngleH)) * cosf(Radiansf(m_camera.CamAngleV)) * m_camera.CamDistance + m_pointLookAt.Z;
}
void CGLRenderer::CameraMove(CameraDirection direction, double value)
{
	switch (direction)
	{
	case CamDirectionHorizontal:
		m_camera.CamAngleH = fmod(m_camera.CamAngleH + value, 360);
		break;
	case CamDirectionVertical:
		m_camera.CamAngleV = fmod(m_camera.CamAngleV + value, 360);
		break;
	case CamDirectionZoom:
		m_camera.CamDistance += value;
		if (m_camera.CamDistance < 0)
			m_camera.CamDistance = 0;
	default:
		break;
	}
	UpdateCamera();
}
void CGLRenderer::LookAtMove(Axis axis, double value)
{
	switch (axis)
	{
	case AxisX:
		m_pointLookAt.X += value;
		break;
	case AxisY:
		m_pointLookAt.Y += value;
		break;
	case AxisZ:
		m_pointLookAt.Z += value;
		break;
	default:
		break;
	}

	UpdateCamera();
}

void CGLRenderer::DEBUG_DrawCoordSys(double size, double width)
{
	if (!MYDEBUG_ENABLED) return;

	float red_vect[4]   = { 1, 0, 0,1 };
	float green_vect[4] = { 0, 1, 0,1 };
	float blue_vect[4]  = { 0, 0, 1,1 };

	glPushAttrib(GL_CURRENT_BIT); //sacuvaj trenutne setovane atribute (boje)
	GLMaterial oldMat;
	oldMat.LoadCurrent();

	glPushMatrix();
		glBegin(GL_LINES);
			glLineWidth(width);

			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, red_vect);
			glColor4fv(red_vect); // X - Red
			glVertex3f(0, 0, 0);
			glVertex3f(size, 0, 0);

			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, green_vect);
			glColor4fv(green_vect); // Y - Green
			glVertex3f(0, 0, 0);
			glVertex3f(0, size, 0);

			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, blue_vect);
			glColor4fv(blue_vect); // Z - Blue
			glVertex3f(0, 0, 0);
			glVertex3f(0, 0, size);
		glEnd();

		//Isprekidana za negativne ose
		glEnable(GL_LINE_STIPPLE);
		glLineStipple(1, 0x0101);

		glBegin(GL_LINES);
			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, red_vect);
			glColor4fv(red_vect); // X - Red
			glVertex3f(-size, 0, 0);
			glVertex3f(0, 0, 0);

			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, green_vect);
			glColor4fv(green_vect); // Y - Green
			glVertex3f(0, 0, 0);
			glVertex3f(0, -size, 0);

			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, blue_vect);
			glColor4fv(blue_vect); // Z - Blue
			glVertex3f(0, 0, 0);
			glVertex3f(0, 0, -size);
		glEnd();
		glDisable(GL_LINE_STIPPLE);
	glPopMatrix();
	oldMat.Select();
	glPopAttrib();

}
void CGLRenderer::DEBUG_DrawLookAtPosition()
{
	if (!MYDEBUG_ENABLED) return;

	glPushMatrix();
		glTranslated(m_pointLookAt.X, m_pointLookAt.Y, m_pointLookAt.Z);
		DEBUG_DrawCoordSys(0.1, 2);
	glPopMatrix();
}
void CGLRenderer::DEBUG_DrawLight(double x, double y, double z) 
{
	if (!MYDEBUG_ENABLED) return;

	glPushAttrib(GL_CURRENT_BIT);
	GLMaterial old_mat;
	old_mat.LoadCurrent();

	GLMaterial lightMat;
	lightMat.SetAmbient(0.95,0.85,0.25,1);
	lightMat.SetDiffuse(0.95, 0.85, 0.25,1);
	lightMat.SetEmission(0.55,0.45,0.15,1);
	lightMat.SetSpecular(1, 1, 1, 1);
	lightMat.SetShininess(30);
	lightMat.Select();

	glPushMatrix();
		glTranslated(x, y, z);
		DEBUG_DrawCoordSys(0.1, 1);
		glutSolidSphere(0.1, 10, 10);
	glPopMatrix();

	old_mat.Select();
	glPopAttrib();
}
void CGLRenderer::DEBUG_WireMode() 
{
	if (MYDEBUG_WIRE)  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else               glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}
/* ======== DODATNI ZADACI LAB ==========*/
void CGLRenderer::DrawBoxR()
{
	glPushAttrib(GL_CURRENT_BIT);
	glPushMatrix();
	DrawWall();
	glRotated(-90, 0, 1, 0);

	glColor3f(1, 0, 0);
	DrawWall();

	glColor3f(0, 1, 0);
	glRotated(-90, 1, 0, 0);
	DrawWall();


	glColor3f(0.5, 0.5, 0.5);
	glTranslated(5, 0, 0);
	//DEBUG_DrawCoordSys();
	glRotated(-90, 0, 1, 0);
	DrawWall();

	glColor3f(0.7, 0.5, 0.1);
	glTranslated(0, 5, 0);
	//DEBUG_DrawCoordSys();

	glPushMatrix();
	glRotated(90, 1, 0, 0);
	DrawWall();
	glPopMatrix();


	glColor3f(1, 1, 0.0);
	glTranslated(5, 0, 0);
	//DEBUG_DrawCoordSys();
	glRotated(90, 1, 0, 0);
	glRotated(-90, 0, 1, 0);
	DrawWall();
	glPopMatrix();
	glPopAttrib();
}
void CGLRenderer::Piramida(double baseSize, double height)
{
	double steps = 15;
	
	double stepSize = height / steps;
	double stepInnerOffset = (baseSize/2)/(steps + 1);

	glPushMatrix();
	
	glTranslated(0, stepSize / 2, 0);
	for (int i = 0; i < steps; i++)
	{
		DrawBox(baseSize - (2 * i* stepInnerOffset), stepSize, baseSize - (2 *i* stepInnerOffset));
		glTranslated(0, stepSize, 0);
	}
	
	glPopMatrix();

}