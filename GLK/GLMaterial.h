#pragma once
#include "GL\GL.H"

class GLMaterial
{
private:
	float m_vAmbient[4]  =	{ (float) 0.2, (float) 0.2, (float) 0.2, (float) 1.0 };
	float m_vDiffuse[4]  =	{ (float) 0.8, (float) 0.8, (float) 0.8, (float) 1.0 };
	float m_vSpecular[4] =	{ (float) 0.0, (float) 0.0, (float) 0.0, (float) 1.0 }; //1 1 1 1
	float m_vEmission[4] =	{ (float) 0.0, (float) 0.0, (float) 0.0, (float) 1.0 }; //
	float m_fShininess[1] = { (float) 0.0 };

public:
	GLMaterial();
	virtual ~GLMaterial();

	void Select();
	void SetAmbient	 (float r, float g, float b, float a);
	void SetDiffuse	 (float r, float g, float b, float a);
	void SetEmission (float r, float g, float b, float a);
	void SetSpecular (float r, float g, float b, float a);
	void SetShininess(float s);

	void LoadCurrent();
};

