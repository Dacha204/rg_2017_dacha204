#pragma once
#include "GLMaterial.h"
#include "GLTexture.h"
struct Point3D
{
	double X, Y, Z;
};
struct Camera
{
	double CamAngleH;
	double CamAngleV;
	double CamDistance;
};
enum CameraDirection
{
	CamDirectionHorizontal,
	CamDirectionVertical,
	CamDirectionZoom,
};
enum Axis
{
	AxisX,
	AxisY,
	AxisZ
};

class CGLRenderer
{
public:
	CGLRenderer(void);
	virtual ~CGLRenderer(void);
		
	bool CreateGLContext(CDC* pDC);			// kreira OpenGL Rendering Context
	void PrepareScene(CDC* pDC);			// inicijalizuje scenu,
	void Reshape(CDC* pDC, int w, int h);	// kod koji treba da se izvrsi svaki put kada se promeni velicina prozora ili pogleda i
	void DrawScene(CDC* pDC);				// iscrtava scenu
	void DestroyScene(CDC* pDC);			// dealocira resurse alocirane u drugim funkcijama ove klase,

	double LampAngle1;
	double LampAngle2;
	double LampAngle3;

	//'Grid size'. Rectangle made of smaller rectangles
	int GranularityX = 50;
	int GranularityY = 50;

	//DEBUG Flags
	bool MYDEBUG_ENABLED = false;
	bool MYDEBUG_WIRE = false;
	bool MYDEBUG_DEBUG_TEXTURES_LOADED = false;

	//Navigation
	void CameraMove(CameraDirection direction, double value);
	void LookAtMove(Axis axis, double value);
	void PrepareTextures();
	void LookAtReset() { m_pointLookAt = { 0,0,0 }; };

private:
	//Navigation
	Camera  m_camera;
	Point3D m_pointEye;
	Point3D m_pointLookAt;
	
	//Materials
	GLMaterial m_materialWood;
	GLMaterial m_materialLamp;
	GLMaterial m_materialFloor;
	GLMaterial m_materialWall;
	GLMaterial m_materialBulb;

	//Navigation and Debug
	void UpdateCamera();
	void DEBUG_DrawCoordSys(double size = 10.0, double width = 1.0);
	void DEBUG_DrawLookAtPosition();
	void DEBUG_DrawLight(double x = 0, double y = 0, double z = 0);
	void DEBUG_WireMode();

	void DrawTexturedTableTop(double a, double b, double c, int slicesX, int slicesY);

	void DrawTexturedTable();

	void DrawTexturedWall(double wallSize = 5);

	void DrawTexturedWalls(double wallsSize = 5);

	void DrawTexturedFloor(double size = 5);

	void DrawTexturedCylinder(double Radius, double Height, int nSides);

	//Lighting
	void SetLightModel();
	void SetLightMain(double x = 0, double y = 0, double z = 0);
	void SetLightLamp(double x = 0, double y = 0, double z = 0);

	//Materials
	void InitWoodMaterial();
	void InitLampMaterial();
	void InitFloorMaterial();
	void InitWallMaterial();
	void InitBulbMaterial();

	//Textures
	GLTexture m_textureWall;
	GLTexture m_textureFloor;
	GLTexture m_textureWood;

	//Drawing primitives and objects
	void DrawRectangle(double width, double height, int slicesW = 10, int slicesH = 10);
	void DrawBox(double a = 1, double b = 1, double c = 1, int slicesX = 10, int slicesY = 10); //quads
	void DrawBoxQS(double a = 1, double b = 1, double c = 1); //quads + quadstrip
	
	void DrawTable();
	void DrawLamp();
	
	void DrawWall(double wallSize = 5);
	void DrawWalls(double wallsSize = 5);

	void DrawTexturedRect(double dSizeX, double dSizeY, int nSlicesX, int nSlicesY, double nRepeatS, double nRepeatT);

	//Extra tasks
	void DrawBoxR();
	void Piramida(double baseSize, double height);
protected:
	HGLRC	 m_hrc; //OpenGL Rendering Context 

};
